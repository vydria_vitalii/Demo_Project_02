#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=Cygwin-Windows
CND_DLIB_EXT=dll
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/example/client/ClientMain.o \
	${OBJECTDIR}/example/client/IReceiveFile.o \
	${OBJECTDIR}/example/client/ReceiveFileImpl.o \
	${OBJECTDIR}/example/server/ISenderFile.o \
	${OBJECTDIR}/example/server/SenderFileImpl.o \
	${OBJECTDIR}/example/server/ServerMain.o \
	${OBJECTDIR}/socket/BaseSocket.o \
	${OBJECTDIR}/socket/InetSocket.o \
	${OBJECTDIR}/socket/ServerSocket.o \
	${OBJECTDIR}/socket/Socket.o \
	${OBJECTDIR}/socket/SocketAddress.o \
	${OBJECTDIR}/socket/SocketException.o \
	${OBJECTDIR}/socket/StreamSocket.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/demoproject_2.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/demoproject_2.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/demoproject_2 ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/example/client/ClientMain.o: example/client/ClientMain.cpp
	${MKDIR} -p ${OBJECTDIR}/example/client
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/example/client/ClientMain.o example/client/ClientMain.cpp

${OBJECTDIR}/example/client/IReceiveFile.o: example/client/IReceiveFile.cpp
	${MKDIR} -p ${OBJECTDIR}/example/client
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/example/client/IReceiveFile.o example/client/IReceiveFile.cpp

${OBJECTDIR}/example/client/ReceiveFileImpl.o: example/client/ReceiveFileImpl.cpp
	${MKDIR} -p ${OBJECTDIR}/example/client
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/example/client/ReceiveFileImpl.o example/client/ReceiveFileImpl.cpp

${OBJECTDIR}/example/server/ISenderFile.o: example/server/ISenderFile.cpp
	${MKDIR} -p ${OBJECTDIR}/example/server
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/example/server/ISenderFile.o example/server/ISenderFile.cpp

${OBJECTDIR}/example/server/SenderFileImpl.o: example/server/SenderFileImpl.cpp
	${MKDIR} -p ${OBJECTDIR}/example/server
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/example/server/SenderFileImpl.o example/server/SenderFileImpl.cpp

${OBJECTDIR}/example/server/ServerMain.o: example/server/ServerMain.cpp
	${MKDIR} -p ${OBJECTDIR}/example/server
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/example/server/ServerMain.o example/server/ServerMain.cpp

${OBJECTDIR}/socket/BaseSocket.o: socket/BaseSocket.cpp
	${MKDIR} -p ${OBJECTDIR}/socket
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/socket/BaseSocket.o socket/BaseSocket.cpp

${OBJECTDIR}/socket/InetSocket.o: socket/InetSocket.cpp
	${MKDIR} -p ${OBJECTDIR}/socket
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/socket/InetSocket.o socket/InetSocket.cpp

${OBJECTDIR}/socket/ServerSocket.o: socket/ServerSocket.cpp
	${MKDIR} -p ${OBJECTDIR}/socket
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/socket/ServerSocket.o socket/ServerSocket.cpp

${OBJECTDIR}/socket/Socket.o: socket/Socket.cpp
	${MKDIR} -p ${OBJECTDIR}/socket
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/socket/Socket.o socket/Socket.cpp

${OBJECTDIR}/socket/SocketAddress.o: socket/SocketAddress.cpp
	${MKDIR} -p ${OBJECTDIR}/socket
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/socket/SocketAddress.o socket/SocketAddress.cpp

${OBJECTDIR}/socket/SocketException.o: socket/SocketException.cpp
	${MKDIR} -p ${OBJECTDIR}/socket
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/socket/SocketException.o socket/SocketException.cpp

${OBJECTDIR}/socket/StreamSocket.o: socket/StreamSocket.cpp
	${MKDIR} -p ${OBJECTDIR}/socket
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/socket/StreamSocket.o socket/StreamSocket.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
