/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ClientMain.cpp
 * Author: Vitaliy
 *
 */

#include <cstdlib>
#include <memory>

#include "IReceiveFile.h"
#include "ReceiveFileImpl.h"

using namespace std;

static const size_t SIZE_PARAM = 3;

int main(int argc, char** argv) {
    if (argc < SIZE_PARAM) {
        throw std::invalid_argument("invalid param");
    }
    std::shared_ptr<IReceiveFile> recv(new ReceiveFileImpl);
    SocketAddress endpoint(argv[1], stoi(argv[2]));
    recv->reciveFile(endpoint);
    return 0;
}

