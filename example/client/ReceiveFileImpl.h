/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ReceiveFileImpl.h
 * Author: Vitaliy
 *
 * Created on September 9, 2017, 6:09 AM
 */

#ifndef RECEIVEFILEIMPL_H
#define RECEIVEFILEIMPL_H

#include "IReceiveFile.h"
#include <iostream>

class ReceiveFileImpl : public IReceiveFile {
public:
    ReceiveFileImpl() = default;
    ReceiveFileImpl(const ReceiveFileImpl &) = delete;
    ReceiveFileImpl& operator=(const ReceiveFileImpl &) = delete;
    ReceiveFileImpl(const ReceiveFileImpl &&) noexcept {}
    ReceiveFileImpl& operator=(const ReceiveFileImpl &&) noexcept {}
    void reciveFile(const SocketAddress& endpoing) throw(std::runtime_error, SocketException) override;
    virtual ~ReceiveFileImpl() = default;
};

#endif /* RECEIVEFILEIMPL_H */

