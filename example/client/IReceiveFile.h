/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   IReceiveService.h
 * Author: Vitaliy
 *
 * Created on September 7, 2017, 6:04 AM
 */

#ifndef IRECEIVESERVICE_H
#define IRECEIVESERVICE_H

#include "../../socket/SocketException.h"
#include "../../socket/SocketAddress.h"
#include <iostream>

class IReceiveFile {
public:
    IReceiveFile() = default;
    IReceiveFile(const IReceiveFile &) = delete;
    IReceiveFile& operator=(const IReceiveFile &) = delete;
    IReceiveFile(IReceiveFile &&) noexcept = default;
    IReceiveFile& operator=(IReceiveFile &&) noexcept = default;
    virtual ~IReceiveFile() = default;
    virtual void reciveFile(const SocketAddress &endpoing)
    throw (std::runtime_error, SocketException) = 0;
};

#endif /* IRECEIVESERVICE_H */

