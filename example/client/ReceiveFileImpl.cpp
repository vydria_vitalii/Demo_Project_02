/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ReceiveFileImpl.cpp
 * Author: Vitaliy
 * 
 */

#include "../client/ReceiveFileImpl.h"
#include "../../socket/Socket.h"
#include <iostream>
#include <fstream>  

void ReceiveFileImpl::reciveFile(const SocketAddress &endpoing) throw (std::runtime_error, SocketException) {
    std::cout << "clien start" << std::endl;
    Socket soc(endpoing);
    std::ofstream out(soc.receive().c_str(), std::ios::binary);
    if (!out.is_open()) {
        throw std::runtime_error("error open file");
    }
    out << soc.receive();
    std::cout << "clien done" << std::endl;
}

