/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SenderFileImpl.cpp
 * Author: Vitaliy
 * 
 * Created on September 9, 2017, 4:31 AM
 */

#include <ostream>
#include <memory>

#include "../server/SenderFileImpl.h"
#include "../../socket/SocketAddress.h"
#include "../../socket/ServerSocket.h"
#include <fstream>

void SenderFileImpl::sendFile(const SocketAddress &endpoint, const std::string pathFile) throw (std::invalid_argument, SocketException) {
    std::cout << "server start" << std::endl;
    SocketAddress addr("127.2.0.1", 8000);
    ServerSocket server(addr);
    auto client = server.accept();
    std::ifstream file(pathFile, std::ifstream::binary);
    if (!file.is_open()) {
        throw std::invalid_argument("file not open");
    }
    client->send(getFileName(pathFile));
    client->send(file);
    std::cout << "server done" << std::endl;
}
