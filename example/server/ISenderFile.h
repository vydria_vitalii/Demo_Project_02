/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ISenderFile.h
 * Author: Vitaliy
 *
 * Created on September 9, 2017, 4:22 AM
 */

#ifndef ISENDERFILE_H
#define ISENDERFILE_H

#include "../../socket/SocketException.h"
#include "../../socket/SocketAddress.h"

class ISenderFile {
public:
    ISenderFile() = default;
    ISenderFile(const ISenderFile&) = delete;
    ISenderFile(ISenderFile &&) noexcept = default;
    ISenderFile& operator=(const ISenderFile &) = delete;
    ISenderFile& operator=(ISenderFile &) noexcept = default;
    virtual ~ISenderFile() = default;
    virtual void sendFile(const SocketAddress &endpoint, const std::string pathFile)
    throw (std::invalid_argument, SocketException) = 0;

    static std::string getFileName(const std::string& s) throw (std::runtime_error) {
        char sep = '/';
#ifdef _WIN32
        sep = '\\';
#endif
        size_t i = s.rfind(sep, s.length());
        if (i != std::string::npos) {
            return (s.substr(i + 1, s.length() - i));
        }
        throw std::invalid_argument("not fount path");
    }
};
#endif /* ISENDERFILE_H */

