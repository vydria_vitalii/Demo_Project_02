/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SenderFileImpl.h
 * Author: Vitaliy
 *
 * Created on September 9, 2017, 4:31 AM
 */

#ifndef SENDERFILEIMPL_H
#define SENDERFILEIMPL_H

#include "ISenderFile.h"

class SenderFileImpl : public ISenderFile {
public:
    SenderFileImpl() = default;
    SenderFileImpl(const SenderFileImpl &) = delete;

    SenderFileImpl(const SenderFileImpl &&) noexcept {
    }
    SenderFileImpl& operator=(const SenderFileImpl &) = delete;

    SenderFileImpl& operator=(const SenderFileImpl &&) noexcept {
    }
    ~SenderFileImpl() = default;
    void sendFile(const SocketAddress &endpoint, const std::string pathFile) throw (std::invalid_argument, SocketException) override;
};

#endif /* SENDERFILEIMPL_H */

