/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Vitaliy
 *
 * Created on September 7, 2017, 3:43 PM
 */

#include <cstdlib>
#include <iostream>

#include "ISenderFile.h"
#include "SenderFileImpl.h"
#include <memory>
using namespace std;

static const size_t COUNT_PARAM = 4;

/*
 * 
 */
int main(int argc, char *argv[]) {
    if (argc < COUNT_PARAM) {
        throw std::runtime_error("invalid param");
    }
    SocketAddress endpoint(argv[1], stoi(argv[2]));
    std::shared_ptr<ISenderFile> service(new SenderFileImpl());
    service->sendFile(endpoint, argv[3]);
    return 0;
}
