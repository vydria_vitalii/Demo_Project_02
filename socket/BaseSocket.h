/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BaseSocket.h
 * Author: Vitaliy
 *
 * Created on September 7, 2017, 3:09 PM
 */

#ifndef BASESOCKET_H
#define BASESOCKET_H

#include "SocketException.h"
#include "Enum.h"

class BaseSocket {
public:
    explicit BaseSocket(const BlockingMode &mode = BlockingMode::BLOCKING,
            const bool autoClose = true);
    BaseSocket(const BaseSocket&) = delete;
    BaseSocket& operator=(const BaseSocket& right) = delete;
    BaseSocket(BaseSocket &&) noexcept;
    BaseSocket& operator=(BaseSocket&&) noexcept;
    virtual ~BaseSocket();
    virtual void close() throw (SocketException);
    bool isClosed() const;
    bool isAutoClose() const;
    bool isBlocking() const;
protected:
    int getFd() const;
    void setClosed(const bool closed);
    void setFd(const int fd);
    void setNonBlockingSocket() throw (SocketException);
private:
    int setNonBlocking();
    int fd;
    BlockingMode mode;
    bool closed;
    const bool autoClose;
};

#endif /* BASESOCKET_H */

