/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ServerSocket.cpp
 * Author: Vitaliy
 * 
 * Created on September 7, 2017, 3:49 PM
 */

#include "ServerSocket.h"
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>

bool ServerSocket::isBound() const {
    return bound;
}

ServerSocket::ServerSocket(const BlockingMode& mode, const bool autoClose)
: InetSocket(SockType(TypeSocket::TYPE_TCP, ProtocolSocket::TCP), true, mode,
autoClose), bound(false) {
}

ServerSocket::ServerSocket(const SocketAddress& endpoint, const int backlog,
        const BlockingMode& mode, const bool autoClose) throw (SocketException) :
InetSocket(SockType(TypeSocket::TYPE_TCP, ProtocolSocket::TCP), endpoint, true,
mode, autoClose), bound(false) {
    bind(getConstSocketAddress(), backlog);
}

void ServerSocket::lisnten(const int backlog) throw (SocketException) {
    int conn = backlog;
    if (conn < 0 || conn > SOMAXCONN) {
        conn = SOMAXCONN;
    }
    if (::listen(getFd(), backlog) != 0) {
        throw SocketException(__FILE__, __LINE__, "listen failed");
    }
}

void ServerSocket::bind(const SocketAddress& endpoint, const int backlog)
throw (SocketException) {
    if (isClosed()) {
        throw SocketException(__FILE__, __LINE__, "closed socket");
    }
    if (isBound()) {
        throw SocketException(__FILE__, __LINE__, "Already bound or created");
    }
    setSocketAddress(endpoint);
    setup(getConstSocketAddress());
    lisnten(backlog);
    setNonBlockingSocket();
    bound = true;
}

void ServerSocket::bind(const SocketAddress& endpoint) throw (SocketException) {
    bind(endpoint, SOMAXCONN);
}

std::shared_ptr<Socket> ServerSocket::accept(const BlockingMode &mode,
        const bool autoClose) throw (SocketException) {
    if (isClosed()) {
        throw SocketException(__FILE__, __LINE__, "socket is closed");
    }
    if (!isBound()) {
        bind(getConstSocketAddress());
    }
    using std::unique_ptr;
    static const size_t hostLen = 1024;
    static const size_t portLen = 32;
    auto srcHost = std::make_unique<char []>(hostLen);
    auto srcPort = std::make_unique<char []>(portLen);
    memset(srcHost.get(), 0x00, hostLen);
    memset(srcPort.get(), 0x00, portLen);
    int clientSfd;
    std::shared_ptr<Socket> client;
    if (-1 == (clientSfd = accept(getFd(), srcHost.get(), hostLen, srcPort.get(), portLen, 0))) {
        if (isBlocking() && errno != EWOULDBLOCK) {
            throw SocketException(__FILE__, __LINE__,
                    "inet_stream_server::accept() - could not accept new connection on stream server socket!");
        } else {
            client = nullptr;
        }
    } else {
        client.reset(new Socket);
        client->setFd(clientSfd);
        SocketAddress addr(std::string(srcHost.get()),
                std::stoi(std::string(srcPort.get())),
                getConstSocketAddress().getInetAddress());
        client->setSocketAddress(std::move(addr));
    }
    return client;
}

int ServerSocket::accept(int sfd, char* src_host, size_t src_host_len, char*
        src_service, size_t src_service_len, int flags) {
    struct sockaddr_storage client_info;
    int client_sfd;
    struct sockaddr_storage oldsockaddr;
    socklen_t oldsockaddrlen = sizeof (struct sockaddr_storage);
    struct hostent* he;
    void* addrptr;
    size_t in_addrlen;
    uint16_t sport = 0;
    socklen_t addrlen = sizeof (struct sockaddr_storage);
    if (-1 == (client_sfd = ::accept(sfd, (struct sockaddr*) &client_info, &addrlen))) {
        return -1;
    }
    if (src_host_len > 0 || src_service_len > 0) {
        if (flags == 1) {
            flags = NI_NUMERICHOST | NI_NUMERICSERV;
        } else {
            flags = 0;
        }
    }
    if (-1 == getsockname(sfd, (struct sockaddr*) &oldsockaddr, &oldsockaddrlen)) {
        return -1;
    }
    if (oldsockaddrlen > sizeof (struct sockaddr_storage)) {
        return -1;
    }
    if (oldsockaddr.ss_family == AF_INET) {
        addrptr = &(((struct sockaddr_in*) &client_info)->sin_addr);
        in_addrlen = sizeof (struct in_addr);
        sport = ntohs(((struct sockaddr_in*) &client_info)->sin_port);
    } else if (oldsockaddr.ss_family == AF_INET6) {
        addrptr = &(((struct sockaddr_in6*) &client_info)->sin6_addr);
        in_addrlen = sizeof (struct in6_addr);
        sport = ntohs(((struct sockaddr_in6*) &client_info)->sin6_port);
    }
    he = gethostbyaddr((const char *) addrptr, in_addrlen, oldsockaddr.ss_family);
    strncpy(src_host, he->h_name, src_host_len);
    snprintf(src_service, src_service_len, "%u", sport);
    return client_sfd;
}

