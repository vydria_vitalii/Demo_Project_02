/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Socket.h
 * Author: Vitaliy
 *
 * Created on September 7, 2017, 3:55 PM
 */

#ifndef SOCKET_H
#define SOCKET_H

#include "InetSocket.h"
#include "StreamSocket.h"

class Socket : public InetSocket, public StreamSocket {
public:
    Socket(const BlockingMode& mode = BlockingMode::BLOCKING, const bool autoClose = true);
    Socket(const SocketAddress &endpoint, const BlockingMode& mode = BlockingMode::BLOCKING,
            const bool autoClose = true) throw (SocketException);
    Socket(const Socket &) = delete;
    Socket &operator=(Socket &) = delete;
    //TODO: написать swap конструкции

    ~Socket();
    void close() throw(SocketException) override;
    void connect(const SocketAddress & = SocketAddress()) throw (SocketException);
    bool isConnected() const;
    friend class ServerSocket;
private:
    bool connected;
};

#endif /* SOCKET_H */
