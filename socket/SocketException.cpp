/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SocketException.cpp
 * Author: Vitaliy
 * 
 * Created on September 7, 2017, 3:23 PM
 */

#include "SocketException.h"
#include <sstream>

SocketException::SocketException(const SocketException& orig)
: std::exception(orig),
error(orig.error), message(message) {
}

SocketException::SocketException(const std::exception &ex)
: std::exception(ex) {
}

SocketException::SocketException(const std::string &file, const int line,
        const std::string &message, std::exception &ex) : std::exception(ex) {
    initMessage(file, line, message);

}

const char* SocketException::what() const noexcept {
    return message.c_str();
}

SocketException::~SocketException() {
}

int SocketException::getError() const {
    return this->error;
}

void SocketException::initMessage(const std::string &file, const int line,
        const std::string &message) {
    std::ostringstream mess;
    error = errno;
    mess << line << ":" << file << ": " << message << " " << std::exception::what();
    this->message = mess.str();
}

SocketException::SocketException(SocketException &&other)
: std::exception(std::move(other)), error(other.error), message(std::move(other.message)) {
    other.error = 0;
}

SocketException &SocketException::operator=(SocketException&& other) {
    if (this == &other) {
        return *this;
    }
    exception::operator=(std::move(other));
    error = other.error;
    other.error = 0;
    message = std::move(other.message);
}

SocketException::SocketException(const std::string &file, const int line,
        const std::string &message) {
    initMessage(file, line, message);
}