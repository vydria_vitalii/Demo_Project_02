/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Socket.cpp
 * Author: Vitaliy
 * 
 * Created on September 7, 2017, 3:55 PM
 */

#include "Socket.h"
#include <memory>
#include <iostream>

Socket::Socket(const BlockingMode& mode, const bool autoClose)
: InetSocket(SockType(TypeSocket::TYPE_TCP, ProtocolSocket::TCP), false, mode, autoClose),
connected(false) {
}

Socket::Socket(const SocketAddress& endpoint, const BlockingMode &mode,
        const bool autoClose) throw (SocketException) : InetSocket(SockType(TypeSocket::TYPE_TCP,
ProtocolSocket::TCP), endpoint, false, mode, autoClose),
connected(false) {
    connect(getConstSocketAddress());
}

Socket::~Socket() {
    if (isAutoClose()) {
        connected = false;
    }
}

void Socket::close() throw (SocketException) {
    connected = false;
    StreamSocket::close();
}

void Socket::connect(const SocketAddress &endpoint) throw (SocketException) {
    if (isClosed()) {
        throw SocketException(__FILE__, __LINE__, "Socket is closed");
    }
    if (connected) {
        throw SocketException(__FILE__, __LINE__, "already connected");
    }
    if (std::addressof(getConstSocketAddress()) != std::addressof(endpoint)
            && getConstSocketAddress() != endpoint) {
        setSocketAddress(endpoint);
    }
    setSocketAddress(endpoint);
    setNonBlockingSocket();
    setup(getConstSocketAddress());
    connected = true;
}

bool Socket::isConnected() const {
    return connected;
}

