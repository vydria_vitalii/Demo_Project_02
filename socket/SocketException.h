/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SocketException.h
 * Author: Vitaliy
 *
 * Created on September 7, 2017, 3:23 PM
 */

#ifndef SOCKETEXCEPTION_H
#define SOCKETEXCEPTION_H

#include <iostream>

class SocketException : public std::exception {
public:
    explicit SocketException(const std::string &file, const int line,
            const std::string &message, std::exception &ex);
    explicit SocketException(const std::string &file, const int line,
            const std::string &message);
    explicit SocketException(const std::exception &ex);
    SocketException(const SocketException &orig);
    SocketException(SocketException &&other);
    SocketException& operator=(SocketException &&other);
    int getError() const;
    virtual ~SocketException();
    virtual const char *what() const noexcept override;
private:
    void initMessage(const std::string &file, const int line,
            const std::string &message);
    int error;
    mutable std::string message;
};
#endif /* SOCKETEXCEPTION_H */

