/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SocketAddress.h
 * Author: Vitaliy
 *
 * Created on September 7, 2017, 3:26 PM
 */

#ifndef SOCKETADDRESS_H
#define SOCKETADDRESS_H

#include "Enum.h"
#include <iostream>
#include <tuple>

class SocketAddress {
public:
    explicit SocketAddress();
    explicit SocketAddress(const char *host, const int port, InetAddress family = InetAddress::IPv4);
    explicit SocketAddress(const std::string &host, const int port, InetAddress family = InetAddress::IPv4);
    explicit SocketAddress(const SocketAddress& other);
    SocketAddress& operator=(const SocketAddress& other);
    SocketAddress(SocketAddress&& other) noexcept;
    virtual ~SocketAddress();
    SocketAddress& operator=(SocketAddress&& other) noexcept;
    std::string getHost() const;
    int getPort() const;
    InetAddress getInetAddress() const;
    const std::string &getConstHost() const;
    friend bool operator==(const SocketAddress& lhs, const SocketAddress& rhs);
    friend bool operator!=(const SocketAddress& lhs, const SocketAddress& rhs);
    friend bool operator<(const SocketAddress& lhs, const SocketAddress& rhs);
    friend bool operator<=(const SocketAddress& lhs, const SocketAddress& rhs);
    friend bool operator>(const SocketAddress& lhs, const SocketAddress& rhs);
    friend bool operator>=(const SocketAddress& lhs, const SocketAddress& rhs);
private:
    std::string host;
    int port;
    InetAddress addr;
public:
    static const int DEFALT_PORT;
    static const char *DEFAULT_HOST;
};
#endif /* SOCKETADDRESS_H */

