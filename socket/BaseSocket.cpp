/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "BaseSocket.h"
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>

BaseSocket::~BaseSocket() {
    try {
        if (autoClose) {
            close();
        }
    } catch (...) {
        //skip exception
    }
}

BaseSocket::BaseSocket(BaseSocket &&orig) noexcept
: fd(orig.fd), autoClose(orig.autoClose), mode(orig.mode), closed(orig.closed) {
}

BaseSocket &BaseSocket::operator=(BaseSocket&& other) noexcept {
    if (this == &other) {
        return *this;
    }
    mode = other.mode;
    fd = other.fd;
    closed = other.closed;
    return *this;
}

BaseSocket::BaseSocket(const BlockingMode &mode, const bool autoClose)
: fd(-1), autoClose(autoClose), mode(mode), closed(false) {
}

void BaseSocket::close() throw (SocketException) {
    if (!closed && fd != -1) {
        if (0 > ::close(fd)) {
            throw SocketException(__FILE__, __LINE__, "error close socket");
        }
        closed = true;
        fd = -1;
    }
}

bool BaseSocket::isAutoClose() const {

    return autoClose;
}

void BaseSocket::setClosed(const bool closed) {

    this->closed = closed;
}

int BaseSocket::getFd() const {

    return fd;
}

bool BaseSocket::isClosed() const {

    return this->closed;
}

void BaseSocket::setFd(const int fd) {
    this->fd = fd;
}

void BaseSocket::setNonBlockingSocket() throw (SocketException) {
    if (!isBlocking()) {
        if (setNonBlocking() == -1) {
            throw SocketException(__FILE__, __LINE__, "Error set non-blocking mode socket");
        }
    }
}

int BaseSocket::setNonBlocking() {
    int res = -1;
    int flag;
#if defined(O_NONBLOCK)
    if (-1 == (flag = fcntl(getFd(), F_GETFL, 0))) {
        flag = 0;
    }
    res = fcntl(getFd(), F_SETFL, &flag);
#else
    flag = 1;
    res ioctl(getFd(), FIOBIO, &flag);
#endif
    return res;
}

bool BaseSocket::isBlocking() const {
    return static_cast<std::underlying_type<BlockingMode>::type> (mode);
}
