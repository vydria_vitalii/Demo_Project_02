/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SocketAddress.cpp
 * Author: Vitaliy
 * 
 * Created on September 7, 2017, 3:26 PM
 */

#include "SocketAddress.h"

const int SocketAddress::DEFALT_PORT = 8080;
const char *SocketAddress::DEFAULT_HOST = "0.0.0.0";

SocketAddress::SocketAddress()
: host(DEFAULT_HOST), port(DEFALT_PORT), addr(InetAddress::IPv4) {
}

SocketAddress::SocketAddress(const char* host, const int port, InetAddress family)
: host(host), port(port), addr(family) {
}

SocketAddress::SocketAddress(const std::string &host, const int port, InetAddress family)
: host(host), port(port), addr(family) {
}

SocketAddress::SocketAddress(const SocketAddress& other)
: host(other.host),
port(other.port),
addr(other.addr) {
}

SocketAddress& SocketAddress::operator=(const SocketAddress& other) {
    if (this == &other)
        return *this;
    host = other.host;
    port = other.port;
    addr = other.addr;
    return *this;
}

SocketAddress::~SocketAddress() {
}

SocketAddress::SocketAddress(SocketAddress&& other) noexcept :
host(move(other.host)),
port(other.port),
addr(other.addr) {
    other.host = -1;
    other.host = -1;
    other.addr = InetAddress::NONE;
}

SocketAddress& SocketAddress::operator=(SocketAddress&& other) noexcept {
    if (this == &other)
        return *this;
    host = move(other.host);
    other.port = -1;
    other.addr = InetAddress::NONE;
    return *this;
}

std::string SocketAddress::getHost() const {
    return host;
}

int SocketAddress::getPort() const {
    return port;
}

InetAddress SocketAddress::getInetAddress() const {
    return addr;
}

const std::string &SocketAddress::getConstHost() const {
    return host;
}

bool operator==(const SocketAddress& lhs, const SocketAddress& rhs) {
    return std::tie(lhs.host, lhs.port, lhs.addr) == std::tie(rhs.host, rhs.port, rhs.addr);
}

bool operator!=(const SocketAddress& lhs, const SocketAddress& rhs) {
    return !(lhs == rhs);
}

bool operator<(const SocketAddress& lhs, const SocketAddress& rhs) {
    return std::tie(lhs.host, lhs.port, lhs.addr) < std::tie(rhs.host, rhs.port, rhs.addr);
}

bool operator<=(const SocketAddress& lhs, const SocketAddress& rhs) {
    return !(rhs < lhs);
}

bool operator>(const SocketAddress& lhs, const SocketAddress& rhs) {
    return rhs < lhs;
}

bool operator>=(const SocketAddress& lhs, const SocketAddress& rhs) {
    return !(lhs < rhs);
}
