/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   StreamSocket.cpp
 * Author: Vitaliy
 * 
 * Created on September 8, 2017, 11:15 PM
 */

#include "StreamSocket.h"
#include <chrono>
#include <thread>
#include <sstream>

#include <string>
#include <unistd.h>
#include <string.h>
#include <memory>
#include <sys/socket.h>

StreamSocket::StreamSocket() : shutIn(false), shutOut(false),
sizeOfPackages(2048), bytesForPackageSize(16) {
}

bool StreamSocket::isShutdownInput() const {
    return shutIn;
}

bool StreamSocket::isShutdownOutput() const {
    return shutOut;
}

void StreamSocket::shutdownInput() throw (SocketException) {
    if (isClosed()) {
        throw SocketException(__FILE__, __LINE__, "Socket is closed");
    }
    if (getFd() == -1) {
        throw SocketException(__FILE__, __LINE__, "Socket is not created");
    }
    if (isShutdownInput()) {
        throw SocketException(__FILE__, __LINE__, "Socket input is already shutdown");
    }
    if (0 > ::shutdown(getFd(), SHUT_RD)) {
        throw SocketException(__FILE__, __LINE__, "Could not shutdown socket");
    }
}

void StreamSocket::shutdownOutput() throw (SocketException) {
    if (isClosed()) {
        throw SocketException(__FILE__, __LINE__, "Socket is closed");
    }
    if (getFd() == -1) {
        throw SocketException(__FILE__, __LINE__, "Socket is not created");
    }
    if (isShutdownOutput()) {
        throw SocketException(__FILE__, __LINE__, "Socket output is already shutdown");
    }
    if (0 > ::shutdown(getFd(), SHUT_WR)) {
        throw SocketException(__FILE__, __LINE__, "Could not shutdown socket");
    }
}

size_t StreamSocket::getBytesForPackageSize() const {
    return bytesForPackageSize;
}

size_t StreamSocket::getSizeOfPackages() const {
    return sizeOfPackages;
}

void StreamSocket::send(const std::string& message) throw (SocketException) {
    std::stringstream ss;
    ss << message;
    send(ss);
}

void StreamSocket::close() throw (SocketException) {
    std::exception_ptr eptr;
    destroy();
    BaseSocket::close();
}

StreamSocket::~StreamSocket() {
    if (isAutoClose()) {
        destroy();
    }
}

void StreamSocket::destroy() {
    if (getFd() != -1) {
        ::shutdown(getFd(), SHUT_RDWR);
    }
}

void StreamSocket::send(std::istream &stream) throw (SocketException) {
    if (isClosed()) {
        throw SocketException(__FILE__, __LINE__, "Socket is closed");
    }
    if (getFd() == -1) {
        throw SocketException(__FILE__, __LINE__, "Socket is not created");
    }
    if (shutOut) {
        throw SocketException(__FILE__, __LINE__, "Socket output is already shutdown");
    }

    char buffer[sizeOfPackages];
    int streamsize;
    std::stringstream ss;
    std::string strSize;

    stream.seekg(0, std::ios::end);
    streamsize = stream.tellg();
    stream.seekg(std::ios::beg);

    ss << streamsize;
    strSize = ss.str();

    while (strSize.size() < bytesForPackageSize) {
        strSize = "0" + strSize;
    }
    int result = ::send(getFd(), strSize.c_str(), this->bytesForPackageSize, MSG_NOSIGNAL);
    if (result == -1) {
        throw SocketException(__FILE__, __LINE__, "Send package size failed!");
    }
    for (size_t i = 0; i < streamsize / this->sizeOfPackages; i++) {
        stream.read(buffer, sizeOfPackages);
        result = ::send(getFd(), buffer, sizeOfPackages, MSG_NOSIGNAL);
        if (result == -1) {
            throw SocketException(__FILE__, __LINE__, "Send message failed!");
        }
        memset(buffer, 0x00, sizeOfPackages);
    }
    int p = streamsize % sizeOfPackages;
    if (p != 0) {
        char buff[p];
        stream.read(buff, p);
        result = ::send(getFd(), buff, p, MSG_NOSIGNAL);
        if (result == -1) {
            throw SocketException(__FILE__, __LINE__, "Send message tail failed!");
        }
    }
}

std::string StreamSocket::receive() throw (SocketException) {
    if (isClosed()) {
        throw SocketException(__FILE__, __LINE__, "Socket is closed");
    }
    if (getFd() == -1) {
        throw SocketException(__FILE__, __LINE__, "Socket is not created");
    }
    if (shutIn) {
        throw SocketException(__FILE__, __LINE__, "Socket input is already shutdown");
    }

    char buffer[bytesForPackageSize];
    int result = ::recv(getFd(), buffer, bytesForPackageSize, MSG_NOSIGNAL);
    if (result == -1) {
        if (isBlocking() && errno != EWOULDBLOCK) {
            throw SocketException(__FILE__, __LINE__, "Error while reading!");
        } else {
            return "";
        }
    }
    std::string messageSize(buffer, bytesForPackageSize);
    std::stringstream ss;
    int n;
    ss << messageSize;
    ss >> n;

    std::string message;
    size_t nbLoop = n % sizeOfPackages == 0 ? n / sizeOfPackages : n / sizeOfPackages + 1;
    for (size_t i = 0; i < nbLoop;) {
        auto buff = std::make_unique<char[]>(sizeOfPackages);
        result = ::recv(getFd(), buff.get(), sizeOfPackages, MSG_NOSIGNAL);
        if (result < 0) {
            if (isBlocking() && errno != EWOULDBLOCK) {
                throw SocketException(__FILE__, __LINE__, "Error while reading!");
            }
            std::this_thread::yield();
            continue;
        } else {
            i++;
        }
        message += std::string(buff.get(), result);
    }
    return message;
}

void StreamSocket::setBytesForPackageSize(const size_t size) {
    this->bytesForPackageSize = size;
}

void StreamSocket::setSizeOfPackages(const size_t size) {
    this->sizeOfPackages = size;
}


