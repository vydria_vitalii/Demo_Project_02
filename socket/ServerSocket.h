/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ServerSocket.h
 * Author: Vitaliy
 *
 * Created on September 7, 2017, 3:49 PM
 */

#ifndef SERVERSOCKET_H
#define SERVERSOCKET_H

#include <memory>

#include "InetSocket.h"
#include "Socket.h"

class ServerSocket : public InetSocket {
public:
    explicit ServerSocket(const BlockingMode& mode = BlockingMode::BLOCKING,
            const bool autoClose = true);
    explicit ServerSocket(const SocketAddress& endpoint, const int backlog = SOMAXCONN,
            const BlockingMode& mode = BlockingMode::BLOCKING,
            const bool autoClose = true) throw (SocketException);
    //TODO: написать swap конструкции

    ServerSocket(const ServerSocket &) = delete;
    ServerSocket &operator=(const ServerSocket &) = delete;
    ~ServerSocket() = default;
    bool isBound() const;
    void bind(const SocketAddress& endpoint, const int backlog) throw (SocketException);
    void bind(const SocketAddress&) throw (SocketException);
    std::shared_ptr<Socket> accept(const BlockingMode &mode = BlockingMode::BLOCKING, const bool autoClose = true) throw (SocketException);
protected:
    void lisnten(const int backlog) throw (SocketException);
    int accept(int sfd, char* src_host, size_t src_host_len, char* src_service,
            size_t src_service_len, int flags);
private:
    bool bound;
};



#endif /* SERVERSOCKET_H */

