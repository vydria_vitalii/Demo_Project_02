/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   InetSocket.h
 * Author: Vitaliy
 *
 * Created on September 7, 2017, 3:43 PM
 */

#ifndef INETSOCKET_H
#define INETSOCKET_H

#include "SocketAddress.h"
#include "BaseSocket.h"

class InetSocket : public virtual BaseSocket {
public:
    explicit InetSocket(const SockType &sockType, const bool bind, const BlockingMode& mode,
            const bool autoClose);
    explicit InetSocket(const SockType &sockType, const SocketAddress& endpoint,
            const bool bind, const BlockingMode& mode,
            const bool autoClose) throw (SocketException);
    InetSocket(InetSocket &&) noexcept;
    InetSocket(const InetSocket& orig) = delete;
    std::string getHost() const;
    std::string getConstHost() const;
    int getPort() const;
    const SocketAddress &getConstSocketAddress() const;
    InetSocket &operator=(InetSocket &&) noexcept;
    InetSocket &operator=(const InetSocket &) = delete;
    ~InetSocket() = default;
protected:
    void setup(const SocketAddress &) throw (SocketException);

    template<class T>
    void setSocketAddress(T &&endpoint) {
        if (std::addressof(this->endpoint) != std::addressof(endpoint)
                && this->endpoint != endpoint) {
            this->endpoint = std::forward<T>(endpoint);
        }
    }
private:
    SocketAddress endpoint;
    SockType sockType;
    //    mutable BlockingMode mode;
    bool bind;
};

#endif /* INETSOCKET_H */

