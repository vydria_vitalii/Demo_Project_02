/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Enum.h
 * Author: Vitaliy
 *
 * Created on September 7, 2017, 3:13 PM
 */

#ifndef ENUM_H
#define ENUM_H

#include <unistd.h>
#include <netinet/in.h>

enum class InetAddress : int32_t {
    IPv4 = AF_INET,
    IPv6 = AF_INET6,
    NONE = -1
};

//enum class ShutdownMode : int32_t {
//    READ = SHUT_RD,
//    WRITE = SHUT_WR,
//    READ_WRITE = SHUT_RDWR,
//    NONE = -1
//};

enum class BlockingMode : bool {
    BLOCKING = true,
    NONBLOCKING = false
};

enum class TypeSocket : int32_t {
    TYPE_TCP = SOCK_STREAM,
    TYPE_UDP = SOCK_DGRAM,
    NONE = -1
};

enum class ProtocolSocket : int32_t {
    TCP = IPPROTO_TCP,
    UDP = IPPROTO_UDP,
    NONE = -1
};

struct SockType {
    SockType(const TypeSocket& type, const ProtocolSocket& protocol);
    TypeSocket type;
    ProtocolSocket protocol;
};

inline SockType::SockType(const TypeSocket& type, const ProtocolSocket& protocol)
: type(type),
protocol(protocol) {
}

#endif /* ENUM_H */

