/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   InetSocket.cpp
 * Author: Vitaliy
 * 
 * Created on September 7, 2017, 3:43 PM
 */

#include <typeinfo>

#include "InetSocket.h"
#include "BaseSocket.h"

#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h> 
#include <stdint.h>
#include <netdb.h> 
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h> 

InetSocket::InetSocket(const SockType &sockType, const bool bind,
        const BlockingMode& mode,
        const bool autoClose) :
BaseSocket(mode, autoClose), endpoint(SocketAddress()), sockType(sockType),
bind(bind) {
}

InetSocket::InetSocket(const SockType &sockType, const SocketAddress& endpoint,
        const bool bind, const BlockingMode& mode,
        const bool autoClose) throw (SocketException)
: BaseSocket(mode, autoClose), endpoint(endpoint), sockType(sockType),
bind(bind) {
}

const SocketAddress& InetSocket::getConstSocketAddress() const {
    return endpoint;
}

//bool InetSocket::isBlocking() const {
//    return static_cast<std::underlying_type<BlockingMode>::type> (mode);
//}

std::string InetSocket::getConstHost() const {
    return endpoint.getConstHost();
}

std::string InetSocket::getHost() const {
    return endpoint.getHost();
}

int InetSocket::getPort() const {
    return endpoint.getPort();
}

//
//void InetSocket::setNonBlockingSocket() throw (SocketException) {
//    if (!isBlocking()) {
//        if (setNonBlocking() == -1) {
//            throw SocketException(__FILE__, __LINE__, "Error set non-blocking mode socket");
//        }
//    }
//}

void InetSocket::setup(const SocketAddress & endpoint) throw (SocketException) {
    struct addrinfo hints;
    memset(&hints, 0x00, sizeof (hints));
    hints.ai_socktype = static_cast<std::underlying_type<TypeSocket>::type> (sockType.type);
    hints.ai_family = static_cast<std::underlying_type<InetAddress>::type> (endpoint.getInetAddress());
    if (bind) {
        hints.ai_flags = AI_PASSIVE;
    }
    int retval;
    struct addrinfo *result = nullptr;

    if (0 != (retval = getaddrinfo(endpoint.getConstHost().c_str(), std::to_string(endpoint.getPort()).c_str(), &hints, &result))) {
        throw SocketException(__FILE__, __LINE__, "bad addres");
    }
    struct addrinfo *result_check = nullptr;
    int sfd = -1;
    for (result_check = result; result_check != NULL; result_check = result_check->ai_next) {
        sfd = ::socket(result_check->ai_family, result_check->ai_socktype, result_check->ai_protocol);
        if (sfd < 0) {
            continue;
        }
        if (bind) {
            retval = ::bind(sfd, result_check->ai_addr, (socklen_t) result_check->ai_addrlen);
        } else {
            retval = ::connect(sfd, result_check->ai_addr, result_check->ai_addrlen);
        }
        if (retval == 0) {
            break;
        } else {
            ::close(sfd);
        }
    }
    freeaddrinfo(result);
    if (result_check == NULL) {
        throw SocketException(__FILE__, __LINE__, "error setup socket");
    }
    setFd(sfd);
}

InetSocket::InetSocket(InetSocket && other) noexcept
: BaseSocket(std::move(other)), endpoint(std::move(other.endpoint)), sockType(other.sockType),
bind(other.bind) {
    other.sockType.protocol = ProtocolSocket::NONE;
    other.sockType.type = TypeSocket::NONE;
}

InetSocket & InetSocket::operator=(InetSocket && other) noexcept {
    if (this == &other) {
        return *this;
    }
    BaseSocket::operator=(std::move(other));
    endpoint = std::move(other.endpoint);
    sockType = other.sockType;
    //    mode = other.mode;
    bind = other.bind;
    other.sockType.protocol = ProtocolSocket::NONE;
    other.sockType.type = TypeSocket::NONE;
    return *this;
}


