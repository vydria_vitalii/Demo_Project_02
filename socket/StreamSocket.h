/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   StreamSocket.h
 * Author: Vitaliy
 *
 * Created on September 8, 2017, 11:15 PM
 */

#ifndef STREAMSOCKET_H
#define STREAMSOCKET_H

#include "BaseSocket.h"
#include <iostream>

class StreamSocket : public virtual BaseSocket {
public:
    StreamSocket();
    StreamSocket(const StreamSocket&) = delete;
    StreamSocket &operator=(const StreamSocket&) = delete;
    virtual ~StreamSocket();
    //move
    virtual void send(const std::string &message) throw (SocketException);
    virtual void send(std::istream &stream) throw (SocketException);
    virtual std::string receive() throw (SocketException);
    void setSizeOfPackages(const size_t size);
    void setBytesForPackageSize(const size_t size);
    size_t getSizeOfPackages() const;
    size_t getBytesForPackageSize() const;
    bool isShutdownInput() const;
    bool isShutdownOutput() const;
    void shutdownInput() throw (SocketException);
    void shutdownOutput() throw (SocketException);
    void close() throw (SocketException) override;

private:
    void destroy();
    bool shutIn;
    bool shutOut;
    size_t bytesForPackageSize;
    size_t sizeOfPackages;
};

#endif /* STREAMSOCKET_H */

